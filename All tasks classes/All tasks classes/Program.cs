﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace All_tasks_classes
{
    abstract class Figure
    {
        abstract public double Area();
        abstract public double Perimeter();
    }

    class Rectangle : Figure
    {
        int height = 0;
        int side = 0;

        public Rectangle(int a, int b)
        {
            height = a;
            side = b;
        }

        public override double Area()
        {
            return height * side;
        }

        public override double Perimeter()
        {
            return (height + side) * 2;
        }

        interface I
        {
            void M();
        }

        abstract class C : I
        {
            public abstract void M();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rect = new Rectangle(5, 3);
            Console.WriteLine("Area of the rectangle is: ", rect.Area());
            Console.WriteLine("Perimeter of the rectangle is: ", rect.Perimeter());
            Console.ReadKey();
        }
    }
}
