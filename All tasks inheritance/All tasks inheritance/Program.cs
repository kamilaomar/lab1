﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace All_tasks_inheritance
{
    class Vehicle
    {
        public double x { get; set; }
        public double y { get; set; }
        public double speed;
        public double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public class Plane : Vehicle
        {
            public double height { get; set; }
            public double passengers { get; set; }
        }

        public class Ship : Vehicle
        {
            public double passengers { get; set; }
            public string pripiska { get; set; }
        }

        public class Car : Vehicle
        {
            public double height { get; set; }

        }
    }

    class Program
    {
        static void Main()
        {

        }
    }

    
}