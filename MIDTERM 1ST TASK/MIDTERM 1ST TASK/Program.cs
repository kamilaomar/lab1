﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Runtime.Serialization;

namespace ConsoleApplication3
{

    class Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public static Point operator +(Point a, Point b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }
    }

    class Line
    {
        public Point a;
        public Point b;
        public Line(Point x, Point y)
        {
            a = x;
            b = y;
        }

        public double LineLength()
        {
            return Math.Sqrt(((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y)));
        }
    }

    class Polygon
    {
        public static void Main()
        {
            ArrayList al = new ArrayList();
            ArrayList al = al.Split(' ');

            al.Add(x);
            al.Add(y);

        }

        public override string ToString()
        {
            return x.ToString() + " " + y.ToString;
        }
        public double GetPerimeter()

        {
            return x + y; 
        }

        public static Polygon operator *(Polygon p, Polygon g)
        {
            return GetPerimeter(p) + GetPerimeter(g); 
        }


    }


    class Program
    {
        static void Main(string[] args)
        {
            Point x = new Point(1, 2);
            Point y = new Point(2, 3);
            Point z = x + y;
            Console.WriteLine(z);
            Console.ReadKey();
            Line k = new Line(Point a, Point b);
            Console.WriteLine(k);
            Console.ReadKey();
            StreamReader sr = new StreamReader("polygon.txt");
            Polygon p = new Polygon();
            Polygon g = new Polygon();
        }
    }
}