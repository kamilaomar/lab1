﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace graphics
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap bm = new Bitmap(s.Width, s.Height);
            Graphics g = Graphics.FromImage(bm);
            float total = 0;
            foreach(PieChartElement e in elements)
            {
                if(e.value < 0)
                {
                    throw new ArgumentException("All elements must have positive values");

                }
                total += e.value;

            }
            Rectangle rect = new Rectangle(1, 1, s.Width - 2, s.Height - 2);
            Pen p = new Pen(Color.Black, 1);
            float startAngle = 0;
            foreach(PieChartElement e in elements)
            {
                float sweepAngle = (e.value / total) * 360;
                g.DrawPie(p, rect, startAngle, sweepAngle);
                startAngle += sweepAngle;
            }
        }
    }
}
