﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    struct BigNumber
    {
        public int[] a;
        public int sz;
        public BigNumber(string s)
        {
            a = new int[1000];
            sz = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }

        public override string ToString()
        {
            string s = "";
            for (int i = sz - 1; i >= 0; i--)
            {
                s += a[i].ToString();
            }
            return s;
        }

      

        public static BigNumber operator *(BigNumber a, int k)
        {

            BigNumber c = new BigNumber("0");
            int d = 0;
            int size = a.sz ;
            c.sz = a.sz;
            

            for(int i=0; i<size; i++)
            {
                c.a[i] = (a.a[i] * k) + d;
                d = c.a[i] / 10;
                c.a[i] %= 10;
            }

            if(d > 0)
            {   
                c.sz += 1;
                c.a[c.sz - 1] = d;
            }

            return c;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            BigNumber a = new BigNumber(Console.ReadLine());
            BigNumber b = new BigNumber(Console.ReadLine());
            Console.WriteLine(a * 5);
            Console.ReadKey();
        }
    }
}