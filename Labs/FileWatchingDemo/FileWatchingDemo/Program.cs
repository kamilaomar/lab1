﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;



namespace FileWatchingDemo
{
    class Program
    {
        public static void ShowDirectory(DirectoryInfo Dir)
        {    
            foreach(DirectoryInfo folders in Dir.GetDirectories())
            {
                ShowDirectory(folders);
                Console.WriteLine("File: {0}", Dir.FullName);
            }

            foreach(FileInfo files in Dir.GetFiles())
            {
                Console.WriteLine("File: {0}", Dir.FullName);
            }
        }
            
        static void Main(string[] args)
        {
            DirectoryInfo Dir = new DirectoryInfo(@"C:\Users\Home\Desktop\1 курс 2 семестр\Programming Technologies");
            ShowDirectory(Dir);
            Console.ReadKey();
        }
    }
}
