﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNumber
{
    struct DNumber 
    {
        public int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }

        public override string ToString()
        {
            return a.ToString() + "+" + b.ToString();
        }


        public static DNumber operator + (DNumber arg1, DNumber arg2)
        {
            arg1.a += arg2.a;
            arg1.b += arg2.b;
          
            return arg1;
        }
   }
    class Program
    {
        static void Main(string[] args)
        {
            DNumber x = new DNumber(1, 2);
            DNumber y = new DNumber (2, 3);
            DNumber z = x + y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
