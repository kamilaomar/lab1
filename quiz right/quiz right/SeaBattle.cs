﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quiz_right
{
    class SeaBattle
    {
        int[,] a = new int[11, 11];
        bool check(int x, int y, int k, bool is_horizontal)
        {
            if (y + k - 1 <= 10)
            {
                for (int i = x - 1; i <= x + 1; i++)
                    for (int j = y - 1; j <= y + k; j++)
                        if (a[i, j] != 0)
                            return false;
            }
            else
                return false;
            
            return true;


        }

        public void set(int x, int y, int k, bool is_horizontal)
        {
            for (int i = y; i < y + k; i++)
                a[x, i] = 1;
        }
    }
}
