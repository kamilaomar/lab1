﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saper
{
    public partial class Form1 : Form
    {
        logic l = new logic(10, 10, 5);
        public Form1()
        {
            InitializeComponent();
            for (int i=1; i<=10; i++)
            {
                for(int j=1; j<=10; j++)
                {
                    MyButton bt = new MyButton(i, j);
                    bt.Click += new System.EventHandler(button1_Click);
                    bt.Size = new Size(30, 30);
                    bt.Location = new Point(i * 30, j * 30);
                    this.Controls.Add(bt);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}