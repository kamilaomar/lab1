﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeOrNot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool prime = false;
            int n = int.Parse(textBox1.Text);
            for(int i = 2; i < n; i++)
            {
                if(n % i == 0)
                {
                    prime = true;
                    label1.Text = "not prime";
                }
            }


            if (prime == false)
                label1.Text = "prime";
           
        }
    }
}