﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Leva
{

    class Program
    {
        static int Levenshtein(string a, string b)
        {
            int k = a.Length;
            int l = b.Length;
            int[,] arr = new int[k+1, l+1];
            if (k == 0)
            {
                return l;
            }
            else if (l == 0)
            {
                return k;
            }

            for (int i = 0; i <= k; i++)
            {
                arr[i, 0] = i;
            }
             
            for (int j = 0; j<=l;j++)
            {
                arr[0, j] = j;
            }

            for(int i=1; i<=k; i++)
            {
                for(int j = 1; j <= l; j++)
                {
                    int steps = (a[i - 1] == b[j - 1] ? 0 : 2);
                    arr[i, j] = Math.Min(Math.Min(arr[i - 1, j] + 1, arr[i, j - 1] + 1), arr[i - 1, j - 1] + steps);
                }
            }
            return arr[k, l];
        }
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\Users\Home\Desktop\1 курс 2 семестр\Programming Technologies\Leva\words.txt");
            StreamReader input = new StreamReader(@"C:\Users\Home\Desktop\1 курс 2 семестр\Programming Technologies\Leva\input.txt");
            StreamWriter sw = new StreamWriter(@"C:\Users\Home\Desktop\1 курс 2 семестр\Programming Technologies\Leva\output.txt");

            string[] array = sr.ReadToEnd().Split(' ');

            List<string> words = new List<string>();

            string vocab;
            while((vocab = sr.ReadLine()) != null )
            {
                words.Add(vocab);
            }

            for(int i=0; i<array.Length; i++)
            {
                string x = words[i].ToLower();
                bool right = false;
                int mindist = -1;
                string tmp = " ";
                for(int j = 0; j < words.Count; j++)
                {
                    string y = words[j];
                    int dist = Levenshtein(x, y);
                    if (dist == 0)
                        right = true;
                    else if(mindist == -1 || mindist > dist)
                    {
                        mindist = dist;
                        tmp = y;
                    }


                }

                if(right == true)
                {
                    sw.Write(array[i] + " ");
                    Console.WriteLine(array[i] + " : is right!");
                }
                else
                {
                    sw.Write(tmp + " ");
                    Console.WriteLine(array[i] + " It will be more correct if " + tmp);
                }
            }
            Console.ReadKey();
            sr.Close();
            input.Close();
            sw.Close();
        }
    }
}
