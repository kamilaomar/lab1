﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace regex
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Company Name: ");
            string s = Console.ReadLine();
            string c = Console.ReadLine();
    

            if(Regex.IsMatch(c, @"^\[a-z]$"))
            {
                Console.WriteLine("Right company name");
                Console.ReadKey();
            }
            if(Regex.IsMatch(s, @"^\+[7]?\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$"))
            {
                Console.WriteLine("Right telephone number");
                Console.ReadKey();
            }
            
            else
            {
                Console.WriteLine("Not right");
                Console.ReadKey();
            }
            
        }
    }
}
