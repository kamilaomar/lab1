﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Snake_final_version
{
    public partial class Form1 : Form

    {
        private List<Circle> Snake = new List<Circle>();
        private Circle food = new Circle();

        public Form1()
        {
            InitializeComponent();
            //set settings to default
            new Settings();

            //set game speed and start timer
            timer1.Interval = 1000 / Settings.Speed;
            timer1.Tick += UpdateScreen;
            timer1.Start();
            
            //Start a new game
            StartGame();
        }

        private void StartGame()
        {
            lblGameOver.Visible = false;
            //set settings to default
            new Settings();

            //Create new player object
            Snake.Clear();
            Circle head = new Circle();
            head.x = 10;
            head.y = 5;
            Snake.Add(head);

            label1.Text = Settings.Score.ToString();
            GenerateFood();

            
        }

        private void GenerateFood()
        {
            //Place randomly the food on the game screen
            int maxXPos = pictureBox1.Size.Width / Settings.Width;
            int maxYPos = pictureBox1.Size.Height / Settings.Height;


            Random random = new Random();
            food = new Circle();
            food.x = random.Next(0, maxXPos);
            food.y = random.Next(0, maxYPos);

        }

        private void UpdateScreen(object sender, EventArgs e)
        {
             //Checks for Game over
            if (Settings.GameOver == true)
            {
                //Check if enter is pressed
                if(Input.KeyPressed(Keys.Enter))
                {
                    StartGame();
                    
                }
            }
            else
            {
                if (Input.KeyPressed(Keys.Right) && Settings.direction != Direction.Left)
                    Settings.direction = Direction.Right;
                else  if (Input.KeyPressed(Keys.Left) && Settings.direction != Direction.Right)
                    Settings.direction = Direction.Left;
                else if (Input.KeyPressed(Keys.Up) && Settings.direction != Direction.Down)
                    Settings.direction = Direction.Up;
                else if (Input.KeyPressed(Keys.Down) && Settings.direction != Direction.Up)
                    Settings.direction = Direction.Down;

                MovePlayer();
                 
            }

            pictureBox1.Invalidate();



        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics canvas = e.Graphics;

            if(Settings.GameOver != false)
            {
                //Set color of snake
                Brush snakeColour;

                //Draw snake
                for(int i= 0; i<Snake.Count; i++)
                {
                    if (i == 0)
                        snakeColour = Brushes.Black;   //Draw head
                    else
                        snakeColour = Brushes.Green;   //Rest of body

                    //Draw snake
                    canvas.FillEllipse(snakeColour, new Rectangle(Snake[i].x * Settings.Width,
                                                                       Snake[i].y * Settings.Height,
                                                                       Settings.Width, Settings.Height));

                    canvas.FillEllipse(Brushes.Red, new Rectangle(food.x * Settings.Width,
                                                                       food.y * Settings.Height, Settings.Width, Settings.Height)); 

                }

            }
            else
            {
                string gameOver = "Game over \nYour final score is: " + Settings.Score + "\nPress Enter to try again";
                lblGameOver.Text = gameOver;
                lblGameOver.Visible = true;

            }
        }

        private void MovePlayer()
        {
            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    switch (Settings.direction)
                    {
                        case Direction.Right:
                            Snake[i].x++;
                            break;
                        case Direction.Left:
                            Snake[i].x--;
                            break;
                        case Direction.Up:
                            Snake[i].y--;
                            break;
                        case Direction.Down:
                            Snake[i].y++;
                            break;
                    }
                }
                else
                {
                    //Move body
                    Snake[i].x = Snake[i - 1].x;
                    Snake[i].y = Snake[i - 1].y;
                }
            }
        }
    }
}
