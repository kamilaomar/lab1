﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            Graphics g = this.CreateGraphics();
            Pen pen = new Pen(Color.Violet, 1);
            g.DrawEllipse(pen, 100, 100, 20, 20);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            
            Graphics g = this.CreateGraphics();
            Pen pen = new Pen(Color.Blue, 1);
            g.DrawEllipse(pen, 100, 100, 30, 30);
                
            Graphics gr = this.CreateGraphics();
            Pen pen1 = new Pen(Color.Yellow, 1);
            gr.DrawEllipse(pen1, 100, 100, 40, 40);

            Graphics gra = this.CreateGraphics();
            Pen pen2 = new Pen(Color.Aqua, 1);
            gra.DrawEllipse(pen2, 100, 100, 50, 50);

        }
    }
}
