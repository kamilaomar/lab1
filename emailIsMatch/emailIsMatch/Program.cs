﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace emailIsMatch
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            if(Regex.IsMatch(input, "^([a-z0-9_.-]+)@([a-z0-9_.-]+).([a-z]+)$"))
            {
                Console.WriteLine("True");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("False");
                Console.ReadKey();
            }
        }
    }
}
