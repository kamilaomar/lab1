﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

let rec ismember4 node = function
              |[] -> false
              |(x :: list) -> if x = node then true  
                              else ismember4 node list

let rec assoc node = function
            |[] -> []
            |(v, us)::k -> if node = v then us 
                           else assoc node k

let topsort graph = 
        let rec explore visited node = 
            if ismember4 node visited then visited
            else let edges = assoc node graph
                 let visited' = List.fold explore visited edges
                 node :: visited'
        List.fold explore [] (List.map fst graph)     

let myexample = [(0, [3]);
                (1, [0;2]);
                (2,[]);
                (3,[2]);
                (4,[3])] 

let rec ismember5 node = function
                         |[] -> false
                         |((x, c):: list) -> if x = node then true 
                                             else ismember5 node list 

let rec assoc1 node = function
                     |(v, us)::k -> if node = v then us 
                                    else assoc1 node k 

let rec count edges = function
    | [] -> []
    | ((x, c) :: list) -> if ismember4 x edges then c :: count edges list
                          else count edges list

let layer graph = 
       let rec explore visited node = 
          if ismember5 node visited then visited
          else let nodes = assoc1 node graph
               let visited' = List.fold explore visited nodes
               let count' = List.fold max 0 (count nodes visited')
               (node, count' + 1) :: visited'
       List.fold explore [] (List.map fst graph) 


let rec assoc2 node = function
                    |[] -> []
                    |((v, c):: us)::k -> if node = v then c 
                                         else assoc2 node k
   (*                                      
let jump u v = 
                if u >= v then layer u - layer v
                else layer v - layer u
                *)
let jumps graph = 
        let layers = layer graph
        List.map (fun (n, ts) -> 
                  let layersource = assoc1 n layers
                  (n, List.map(fun t -> 
                                        (t, layersource - assoc1 t layers)
                               )ts)
                  )
                  graph

        
  
let rec insert_edge graph (u,v) =
                                 match graph with 
                                 |[] -> [(u, [v])]
                                 |(n, ts)::g -> if n=u then (n, v::ts)::g
                                                else (n, ts)::insert_edge g (u,v)
let rec transpose = function 
                        |[] -> []
                        |(n, t::ts)::g -> insert_edge (transpose((n, ts)::g)) (t, n)
                        |(n, [])::g -> transpose g

let minjump graph = function 
            let layers = layer graph
            List.map(fun(n,ts) ->
                     let layersource = assoc1 n layers)
                     (n, List.map(fun -> 
                                        (t, layersource - assoc1 t layers)
                                  )ts)
                    )
                    graph