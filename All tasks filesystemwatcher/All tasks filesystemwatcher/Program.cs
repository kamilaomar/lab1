﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace All_tasks_filesystemwatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Filter("*.txt");
            watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
            watcher.Path = @"C:\Users\Home\Desktop\1 курс 2 семестр";
        }

        static void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("A new txt file has been created!");

        }
    }
}
    