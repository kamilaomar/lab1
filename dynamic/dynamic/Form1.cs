﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dynamic
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int x = 200;
        int y = 200;
        private void button1_Click(object sender, EventArgs e)
        {
            Label label = new Label();
            label.Text = "Dynamic";
            x += 70;
            label.Location = new Point(x, y);
            this.Controls.Add(label);
        }
    }
}