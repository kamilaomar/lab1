﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ENDTERM_2nd_task
{
    public partial class Form1 : Form
    {
        MyButton[,] btn = new MyButton[8, 8];
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    btn[i, j] = new MyButton(i, j);

                    btn[i, j].Click  += new EventHandler(btn_click);
                    btn[i, j].BackColor = Color.White;
                    btn[i, j].Size = new Size(40, 40);
                    btn[i,j].Location = new Point(i * 40, j * 40);
                    this.Controls.Add(btn[i, j]);
                }
            }
        }

        public void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            btn[b.x, b. y].BackColor = Color.White;



            if (btn[b.x - 2, b.y].BackColor == Color.Black)
            {
                btn[b.x - 2, b.y].BackColor = Color.White;
            }

            else
                btn[b.x - 2, b.y].BackColor = Color.Black;


            if (btn[b.x + 2, b.y].BackColor == Color.Black)
            {
                btn[b.x + 2, b.y].BackColor = Color.White;
            }

            else
                btn[b.x + 2, b.y].BackColor = Color.Black;


            if (btn[b.x, b.y - 2].BackColor == Color.Black)
            {
                btn[b.x, b.y - 2].BackColor = Color.White;
            }

            else
                btn[b.x, b.y - 2].BackColor = Color.Black;


            if (btn[b.x, b.y + 2].BackColor == Color.Black)
            {
                btn[b.x, b.y + 2].BackColor = Color.White;
            }

            else
                btn[b.x, b.y + 2].BackColor = Color.Black;

            if (btn[b.x +2 , b.y + 2].BackColor == Color.Black)
            {
                btn[b.x + 2, b.y + 2].BackColor = Color.White;
            }

            else
                btn[b.x + 2, b.y + 2].BackColor = Color.Black;

            if (btn[b.x + 2, b.y + 2].BackColor == Color.Black)
            {
                btn[b.x + 2, b.y + 2].BackColor = Color.White;
            }

            else
                btn[b.x + 2, b.y + 2].BackColor = Color.Black;

            if (btn[b.x + 1, b.y + 2].BackColor == Color.Black)
            {
                btn[b.x + 1, b.y + 2].BackColor = Color.White;
            }

            else
                btn[b.x + 1, b.y + 2].BackColor = Color.Black;

            if (btn[b.x + 2, b.y + 1].BackColor == Color.Black)
            {
                btn[b.x + 2, b.y + 1].BackColor = Color.White;
            }

            else
                btn[b.x + 2, b.y + 1].BackColor = Color.Black;


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
