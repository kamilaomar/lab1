﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        int p = 0; // 0 - right, 1 - left, 2 - up, 3 -down
        int x = 150;
        int y = 150;
        Graphics g;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g = CreateGraphics();
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Aqua, 50);
            g.DrawEllipse(pen, 150, 150, 10, 10);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(p == 0)
            {
                x += 10;
            }
            if (p == 1)
            {
                x -= 10;
            }
            if (p == 2)
            {
                y -= 10;
            }
            if (p == 3)
            {
                y += 10;
            }
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Aqua, 20);
            g.DrawEllipse(pen, x, y, 10, 10);
        }

      

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
                p = 0;
            if (e.KeyCode == Keys.Left)
                p = 1;
            if (e.KeyCode == Keys.Up)
                p = 2;
            if (e.KeyCode == Keys.Down)
                p = 3;
        }
    }
}
