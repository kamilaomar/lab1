﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_enum
{

    public enum Days : int { Monday, Tuesday, Wedenesday, Thursday, Friday, Saturday, Sunday };
    public class Student
    {
        
        public string gender;
        public double gpa;
        public Days[] day = null;
        public int q = 0;

        public Student(string _gender, double _gpa)
        {
            day = new Days[7];
            gender = _gender;
            gpa = _gpa;
        }

        public void add_schedule(Days d)
        {
            day[q] = d;
            q++;
        }
        public override string ToString()
        {
            string dd = "";
            for (int i = 0; i < q; i++)
            {
                dd += day[i];
            }
            return gender + " " + gpa + " " + dd;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student("Male", 3.92);
            s.add_schedule(Days.Friday);
            s.add_schedule(Days.Monday);
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}