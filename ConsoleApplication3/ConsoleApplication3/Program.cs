﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{

    class Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public static Point operator +(Point a, Point b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            Point x = new Point(1, 2);
            Point y = new Point(2, 3);
            Point z = x + y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
