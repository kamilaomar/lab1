﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace All_tasks_Graphics
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g;
            g = this.CreateGraphics();
            Pen pen = new Pen(Color.Gold, 30);
            Brush Blue = new SolidBrush(Color.DodgerBlue);
            Brush Gold = new SolidBrush(Color.Gold);
            g.FillRectangle(Blue, 10, 10, 450, 250);
            g.DrawLine(pen, 10, 120, 460, 120);
            g.DrawLine(pen, 110, 10, 110, 260);
        }
    }
}
