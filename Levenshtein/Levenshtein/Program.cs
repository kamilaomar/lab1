﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    class Levenshtein
    {
        public static int Calculate(string a, string b)
        {
            int k = a.Length;
            int l = b.Length;
            int [,] arr = new int[k + 1, l + 1];

            if (k == 0)
            {
                return l;
            }

            if (l == 0)
            {
                return k;
            }
            
            for (int i = 0; i <= k; i++)
            {
                arr[i, 0] = i;
            }

            for (int j = 0; j <= l; j++)
            {
                arr[0, j] = j;
            }

            for(int i = 1; i <= k; i++)
            {
                for(int j = 1; i <= l; j++)
                {
                    int steps = (a[i - 1] == b[j - 1] ? 0 : 2);
                    arr[i, j] = Math.Min(Math.Min(arr[i, j - 1] + 1, arr[i - 1, j] + 1), arr[i - 1, j - 1] + steps);
                }
            }

            return arr[k, l];
        }
    }
    class Program
    {
        static void Main()
        {
            string a = "stock";
            string b = "saock";
            Console.WriteLine(Levenshtein.Calculate(a,b));
            Console.ReadKey();
        }
    }
}
