﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace All_tasks_serialization
{
    class Student
    {
        public bool gender {get; set; }
        public double gpa {get; set; } 
        [XmlIgnore]
        public string name {get; set; }
        public string surname {get; set; }

        public Student(string _gender, double _gpa, string _name, string _surname)
        {
            gpa = _gpa;
            name = _name;
            surname = _surname;
        }

        public void Serialize(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var XML = new XmlSerializer(typeof(Student));
                XML.Serialize(stream, this);
            }
        }

        public static Student LoadFromFile(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var XML = new XmlSerializer(typeof(Student));
                return (Student)XML.Deserialize(stream);
            }
        }
    }

    
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            Student.gender = true;
            Student.name = "Kamila";
            Student.surname = "Omar";
            Student.Serialize("xmlfile.xml");
            Console.ReadKey();
        }
    }
}
